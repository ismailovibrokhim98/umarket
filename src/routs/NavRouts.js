import React from 'react'
import { Switch, Route, Link ,BrowserRouter } from 'react-router-dom'
import Main from '../components/Main'
import Comparison from '../components/Pages/comparison/Comparison'
import Deliver from '../components/Pages/deliver/Deliver'
import Korzina from '../components/Pages/korzina/Korzina'
import Market from '../components/Pages/market/Market'
import Oform from '../components/Pages/oformlinya/Oform'
import Review from '../components/Pages/review/Review'

function NavRouts() {
    return (
        <div>
                
                <BrowserRouter>
                    
                    <Switch>
                        <Route path="/market" component={Market} />
                        <Route path="/review" component={Review} />
                        <Route path="/deliver" component={Deliver} />
                        <Route path="/korzina" component={Korzina} />
                        <Route path="/oform" component={Oform} />
                        <Route path="/comparison" component={Comparison} />
                        <Route path="/" component={Main} />
                        
                    </Switch>
                
                </BrowserRouter>

        </div>
    )
}

export default NavRouts
