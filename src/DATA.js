export const Popular_products = [
  {
    id: 1,
    name: "Смартфоны",
    slug: "smartfon",
    image: require("./Assets/images/product-1.png"),
  },
  {
    id: 2,
    name: "Мониторы",
    slug: "monitor",
    image: require("./Assets/images/product-2.png"),
  },
  {
    id: 3,
    name: "Компьютеры",
    slug: "kompyutery",
    image: require("./Assets/images/product-3.png"),
  },
  {
    id: 4,
    name: "Аксессуары",
    slug: "aksessuary",
    image: require("./Assets/images/product-4.png"),
  },
];

export const home_products = [
  {
    id: 1,
    name: "Встраиваемая техника",
    slug: "vstraivaemaya-tehnika",
    image: require("./Assets/images/product-5.png"),
  },
  {
    id: 2,
    name: "Пылесосы",
    slug: "pylezs",
    image: require("./Assets/images/product-6.png"),
  },
  {
    id: 3,
    name: "Стиральные машины",
    slug: "stiraly-mashiny",
    image: require("./Assets/images/product-7.png"),
  },
  {
    id: 4,
    name: "Холодильники",
    slug: "holodilniki",
    image: require("./Assets/images/product-8.png"),
  },
  {
    id: 5,
    name: "Кондиционеры",
    slug: "kondicionery",
    image: require("./Assets/images/product-9.png"),
  },
];

export const Best_products = [
  {
    id: 1,
    name: "Samsung Galaxy A41 Red 64 GB",
    price: '3 144 000 сум',
    loan_price: 'От 385 000 сум/12 мес',
    rate: '3',
    slug: "samsung-galaxy-a41-red-64-gb",
    image: require("./Assets/productList/hitprod1.png"),
  },
  {
    id: 2,
    name: "Samsung Galaxy A41 Black 64 GB",
    price: '3 144 000 сум',
    loan_price: 'От 385 000 сум/12 мес',
    rate: '3',
    slug: "samung-galaxy-a41-black-64-gb",
    image: require("./Assets/productList/hitprod1.png"),
  },
  {
    id: 3,
    name: "Samsung Galaxy A41 Red 128 GB",
    price: '4 750 000 сум',
    loan_price: 'От 425 000 сум/12 мес',
    rate: '4',
    slug: "samung-galaxy-a41-red-128-gb",
    image: require("./Assets/productList/hitprod1.png"),
  },
  {
    id: 4,
    name: "Samsung Galaxy A41 Black 128 GB",
    price: '4 750 000 сум',
    loan_price: 'От 425 000 сум/12 мес',
    rate: '4',
    slug: "samung-galaxy-a41-black-64-gb",
    image: require("./Assets/productList/hitprod2.png"),
  }
];
