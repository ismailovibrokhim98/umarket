import React from "react";

const Button = ({ text, valid }) => {
  console.log("valid", valid);
  const isValid = () => {
    switch (valid) {
      case true:
        return "bg-[#1B5BF7] hover:bg-[#0640CE] text-white font-medium  py-2 px-4 rounded focus:outline-none focus:shadow-outline";
      case false:
        return "bg-[#EAE9EF] font-medium py-2 px-4 rounded focus:outline-none focus:shadow-outline disabled cursor-not-allowed";
      default:
        return "bg-gray-500 hover:bg-gray-700 text-white font-medium py-2 px-4 rounded focus:outline-none focus:shadow-outline";
    }
  };
  return (
    <div className="text-center">
      <button disabled={!valid} className={`h-12 ${isValid(valid)} w-full`}>
        {text || "Button"}
      </button>
    </div>
  );
};

export default Button;
