import React, { useState } from "react";
import RegisterModal from "../Modal";
import { useSelector, useDispatch } from "react-redux";
import FInput from "../Form/FInput";
import { toggleLoginModal } from "../../redux/actions/modalAction";
import Button from "../Button";
import { ReactComponent as Facebook } from "../../Assets/icons/Facebook.svg";
import { ReactComponent as Google } from "../../Assets/icons/Google.svg";
import Modal from '../Modal/index'

const Login = () => {
  const dispatch = useDispatch();
  const loginModal = useSelector((state) => state.modal.loginModal);
  console.log('loginModal', loginModal)
  const [hasAccount, setHasAccount] = useState(false);
  const [aggreement, setAggreement] = useState(false);
  return (
    <div>
      <RegisterModal
        open={loginModal}
        handleClose={() => dispatch(toggleLoginModal(false))}
        title={!hasAccount ? "Регистрация" : "Добро пожаловать"}
        subTitle={
          !hasAccount ? "" : "Войдите с вашим номером телефона или паролем"
        }
        width={550}
      >
        {!hasAccount ? (
          <form>
            <FInput placeholder="Имя" />
            <FInput placeholder="Фамилия" />
            <FInput placeholder="Номер телефона" />
            <FInput placeholder="Введите пароль" />
            <FInput placeholder="Подтверждение пароля" />
            <Button valid={aggreement} text={"Зарегистрироваться"} />
            <div className="register__agreement flex justify-between items-center gap-4 mt-6 mx-3">
              <input
                className="h-7 w-7"
                type="checkbox"
                onClick={() => setAggreement(!aggreement)}
              />
              <span className="block">
                Согласен с{" "}
                <span className="text-[#1B5BF7] cursor-pointer">
                  {" "}
                  условиями правил пользования
                </span>{" "}
                торговой площадкой и
                <span className="text-[#1B5BF7] cursor-pointer"> правилами возврата</span>
              </span>
            </div>
            <div className="text-center py-6">
              <span className="text-center">Или зарегистрироваться с</span>
              <div className="flex justify-between items-center gap-6 my-6">
                <div className="bg-[#F6F6F6] py-[14px] cursor-pointer rounded flex-grow">
                  <Google style={{ width: "100%" }} />
                </div>
                <div className="bg-[#F6F6F6] py-4 cursor-pointer rounded flex-grow">
                  <Facebook style={{ width: "100%" }} />
                </div>
              </div>
            </div>
            <div className="hasAccount text-center">
              <span>
                У вас есть аккаунт?{" "}
                <span
                  className="text-[#1B5BF7] cursor-pointer"
                  onClick={() => {
                    console.log("Hey, Welcome");
                    setHasAccount(true);
                  }}
                >
                  Войти
                </span>
              </span>
            </div>
          </form>
        ) : (
          <form>
            <FInput placeholder="Номер телефона" />
            <Button valid={true} text={"Получить код"} />
            <div className="login text-center py-6">
              <span>Или войти с</span>
              <div className="login__social flex justify-between items-center gap-6 my-6">
                <div className="bg-[#F6F6F6] py-[14px] cursor-pointer rounded flex-grow">
                  <Google style={{ width: "100%" }} />
                </div>
                <div className="bg-[#F6F6F6] py-4 cursor-pointer rounded flex-grow">
                  <Facebook style={{ width: "100%" }} />
                </div>
              </div>
            </div>
            <div className="hasAccount text-center">
              <span>
                У вас нет аккаунта?{" "}
                <span
                  className="text-[#1B5BF7] cursor-pointer"
                  onClick={() => {
                    console.log("Hey, Welcome");
                    setHasAccount(false);
                  }}
                >
                  Зарегистрируйтесь
                </span>
              </span>
            </div>
          </form>
        )}
      </RegisterModal>
    </div>
  );
};

export default Login;
