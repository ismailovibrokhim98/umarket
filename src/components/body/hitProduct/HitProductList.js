import React from 'react'
import "./HitProductList.css"
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import hitprod1 from "../../../Assets/productList/hitprod1.png"
import hitprod2 from "../../../Assets/productList/hitprod2.png"
import Rating from '@mui/material/Rating';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { DerectionIcon } from '../../../Assets/icons/icons';
import { HeartIcon, ShopIcon } from '../../../Assets/icons/navIcon/NavIcon';
import shopicon from "../../../Assets/productList/shopicon.svg"
import { Best_products} from '../../../DATA';
import QuickLook from '../productList/quickLook/QuickLook';

function HitProductList({ title }) {

    const [opens, setOpens] = React.useState(false);
    const handleOpens = () => setOpens(true);
    console.log("opens", opens);

    
    if (!Best_products?.length) return null;
    return (
        <>
            <div className="main_HitProductList">
                <div className="cont_HitProductListt">
                    <h2 className="HitProductList_title" >{ title }</h2>
                    
                    <div className='sdasd'>
                        {Best_products &&
                            Best_products.map((BestProducts) => (
                                
                                        <Grid item xs={3} >
                                                <div className="hit_prod">
                                                    <button className="quickSee"  onClick={handleOpens}>Быстрый просмотр</button>
                                                    <div className="hit_card">
                                                        <img src={BestProducts.image} alt="photo" className="hit_img" />
                                                        <p className="hit_title" >{BestProducts.name}</p>
                                                        <h3 className="hit_cost" >{BestProducts.price}</h3>
                                                        <span className="hit_montCost" >{BestProducts.loan_price}</span>
                                                        <Box
                                                            sx={{
                                                                mt: "16px",
                                                                '& > legend': { mt: 2 },
                                                            }}
                                                            >
                                                            <Rating value={BestProducts.loan_rate} />
                                                            <div className="hitcard_bottom">
                                                            <button className="btn_shop">

                                                                        <img src={shopicon} alt="photo" className="shopicon" />
                                                                    <span>В корзину</span>
                                                                </button>
                                                                <div className="asd" >
                                                                    <DerectionIcon/>
                                                                    <span>
                                                                    <HeartIcon />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </Box>
                                                    </div>
                                                </div>
                                        </Grid>
                                    
                        ))}
                    </div>
                </div>
            </div>
        


            <QuickLook opens={opens} close={setOpens}/>                                                


        </>
    )
}

export default HitProductList
