import React from 'react'
import "./TechHome.css"
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import techHome1 from "../../../Assets/productList/techHome1.png"
import techHome2 from "../../../Assets/productList/techHome2.png"
import techHome3 from "../../../Assets/productList/techHome3.png"
import techHome4 from "../../../Assets/productList/techHome4.png"
import techHome5 from "../../../Assets/productList/techHome5.png"

function TechHome() {
    return (
        <>
            <div className="main_TechHome">
                <div className="cont_TechHome">
                    <h2 className="techHome_title" >Техника для дома</h2>

                    <Box sx={{ flexGrow: 1}}>
                        <Grid container spacing={7}>
                            <Grid item xs={2.4}>
                                <div className="techHome_prod">
                                    <div className="techHome_card">
                                        <img src={techHome1} alt="photo" className="techHome_img" />
                                        <p className="techHomeCard_title" >Встраиваемая техника</p>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={2.4}>
                                <div className="techHome_prod">
                                    <div className="techHome_card">
                                        <img src={techHome2} alt="photo" className="techHome_img" />
                                        <p className="techHomeCard_title" >Пылесосы</p>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={2.4}>
                                <div className="techHome_prod">
                                    <div className="techHome_card">
                                        <img src={techHome3} alt="photo" className="techHome_img" />
                                        <p className="techHomeCard_title" >Стиральные машины</p>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={2.4}>
                                <div className="techHome_prod">
                                    <div className="techHome_card">
                                        <img src={techHome4} alt="photo" className="techHome_img" />
                                        <p className="techHomeCard_title" >Холодильники</p>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={2.4}>
                                <div className="techHome_prod">
                                    <div className="techHome_card">
                                        <img src={techHome5} alt="photo" className="techHome_img" />
                                        <p className="techHomeCard_title" >Кондиционеры</p>
                                    </div>
                                </div>
                            </Grid>

                        </Grid>
                    </Box>
                </div>
            </div>
        </>
    )
}

export default TechHome
