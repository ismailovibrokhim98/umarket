import React from 'react'
import "./Brands.css"
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import brands1 from "../../../Assets/productList/brands1.svg"
import brands2 from "../../../Assets/productList/brands2.svg"
import brands3 from "../../../Assets/productList/brands3.svg"
import brands4 from "../../../Assets/productList/brands4.svg"
import brands5 from "../../../Assets/productList/brands5.svg"
import brands6 from "../../../Assets/productList/brands6.svg"

function Brands() {
    return (
        <>
            <div className="main_brand">
                <div className="cont_brands">
                    <h3 className="brands_title" >Популярные бренды</h3>
                    
                    <Box sx={{ flexGrow: 1, mt: "24px" }}>
                        <Grid container spacing={4}>
                            <Grid item xs={1.5}> <button className="btn_brands">Телефоны</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Аксессуары</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Premium</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Спорт</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Игрушки</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Красота</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Книги</button></Grid>
                            <Grid item xs={1.5}> <button className="btn_brands">Обувь</button></Grid>
                        </Grid>
                    </Box>

                    <Box sx={{ flexGrow: 1, mt: "24px" }}>
                        <Grid container spacing={5}>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands1} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands2} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands3} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands3} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands3} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands3} alt="photo" className="brand_img" /> </div>
                            </Grid>
                        </Grid>
                    </Box>

                    <Box sx={{ flexGrow: 1, mt: "10px" }}>
                        <Grid container spacing={5}>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands4} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands5} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands6} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands6} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands6} alt="photo" className="brand_img" /> </div>
                            </Grid>
                            <Grid item xs={2}> 
                                <div className="brand_box" > <img src={brands6} alt="photo" className="brand_img" /> </div>
                            </Grid>
                        </Grid>
                    </Box>
                </div>
            </div>
        </>
    )
}

export default Brands