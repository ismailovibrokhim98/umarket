import React from 'react'
import "./PlayMarket.css"
import umarket from "../../../Assets/productList/umarket.svg"
import googlePlay from "../../../Assets/productList/googlePlay.png"
import appStore from "../../../Assets/productList/appStore.png"
import mobileQR from "../../../Assets/productList/mobileQR.png"

function PlayMarket() {
    return (
        <>
            <div className="main_playMarket">
                <div className="cont_playMarket">
                    <div className="playMarketLeft">
                        <img src={umarket} alt="photo" />
                        <h1 className="zakaz_inf" >Заказывайте через мобильное приложение</h1>
                        <div className="playMarket_downl">
                            <img src={googlePlay} alt="photo" />
                            <img src={appStore} alt="photo" />
                        </div>
                    </div>

                    <img src={mobileQR} alt="photo" />

                    <div className="playMarketRight">
                        <span>Отсканируйте QR-код и установите приложение</span>
                    </div>
                
                </div>
            </div>
        </>
    )
}

export default PlayMarket