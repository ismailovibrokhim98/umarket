import React, { useState } from 'react'
import "./quickLook.css"
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Stack from '@mui/material/Stack';
import CloseIcon from '@mui/icons-material/Close';
import Rating from '@mui/material/Rating';
import quickImg from "../../../../Assets/productList/quckimg.png";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import shopicon from "../../../../Assets/productList/shopicon.svg"
import quickVariant from "../../../../Assets/productList/quickVar.svg"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "769px",
    height: "737px",
    bgcolor: '#F6F6F6',
    boxShadow: 14,
    borderRadius:2,
    p: "40px",
  };
  

  export default function QuickLook(props) {
    

    function closeModal(){
        props.close(false)
    }

    const [alignment, setAlignment] = React.useState('web');

    console.log(" bu quik " + props.opens);


    const handleChange = (event, newAlignment) => {
        setAlignment(newAlignment);
    };

    const [duals, setDuals] = React.useState('dual');

    const handleChangeDual = (event, newDuals) => {
        setDuals(newDuals);
    };
    return (
      <div>
        <Modal
            open={props.opens}
            onClose={closeModal}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                    <Stack 
                        direction="row" 
                        justifyContent="space-between"
                        alignItems="center"
                    >
                        <span className="quickLook_title" >Samsung Galaxy A41</span>
                        <CloseIcon onClick={closeModal} className="quickLook_exitIcon" sx={{color:"#9692B0"}} />
                    </Stack>
                   
                    <Stack 
                        direction="row" 
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{width:"60%", marginTop:"24px"}}
                        spacing={1}
                    >
                        <Rating name="size-large" defaultValue={2} size="large" sx={{color:"#9692B0"}} />
                        <div className="line_hr" ></div>
                        <span className="rate_span" >23 отзывов</span>
                        <div className="line_hr" ></div>
                        <span className="rate_span" >Есть в наличии</span>
                    </Stack>

                <Box 
                    display="flex"
                    justifyContent="flex-start"
                    alignItems="flex-start" sx={{ height:"355px", marginTop:"24px"}}
                    
                        >
                            <Box display="flex" justifyContent="center" alignItems="center"sx={{ width:"455px", height:"354px", background:"#E5E5E5" }}>
                                <img src={quickImg} alt="photo" className="quick_img" />
                            </Box>
                            <Box direction="row" sx={{paddingLeft:"30px"}} >
                                <span className="span_price">$450</span>
                                <p className="p_color" >Цвет</p>
                                <Stack 
                                    direction="row" 
                                    justifyContent="flex-start"
                                    alignItems="center"
                                    spacing={1}
                                    sx={{mt:"8px"}}
                                    >
                                        <div className="radi_div"> <button className="radio_btn"></button> </div> 
                                        <button className="radio_btn bl"></button>
                                        <button className="radio_btn gr"></button>
                                </Stack>
                                <Box sx={{mt:"26px"}}>
                                    <p className="memory_phone_p" >Память</p>
                                    <ToggleButtonGroup
                                        color="standard"
                                        background="#F2F2F2"
                                        value={alignment}
                                        exclusive
                                        onChange={handleChange}
                                        sx={{mt:"4px"}}
                                    >
                                        <ToggleButton sx={{marginRight:"5px"}} className="memory_phone" value="web">64GB</ToggleButton>
                                        <ToggleButton sx={{marginRight:"5px"}} className="memory_phone" value="android">128GB</ToggleButton>
                                        <ToggleButton sx={{marginRight:"5px"}} className="memory_phone" value="ios">256GB</ToggleButton>
                                    </ToggleButtonGroup>
                                </Box>

                                <Box sx={{mt:"26px"}}>
                                    <p className="memory_phone_p" >SIM</p>
                                    <ToggleButtonGroup
                                        color="standard"
                                        background="#F2F2F2"
                                        value={duals}
                                        exclusive
                                        onChange={handleChangeDual}
                                        sx={{mt:"4px"}}
                                    >
                                        <ToggleButton sx={{marginRight:"5px"}} className="phone_dual" value="dual">Dual</ToggleButton>
                                        <ToggleButton sx={{marginRight:"5px"}} className="phone_dual" value="single">Single</ToggleButton>
                                    </ToggleButtonGroup>
                                </Box>

                                <button className="btn_karzinka">
                                    <img src={shopicon} alt="photo" className="shopicon_karzinka" />
                                    <span>Добавить в корзину</span>
                                </button>
                            </Box>
                    </Box>
                
                    <Stack 
                        direction="row" 
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{ width:"64%", mt:"25px"}}
                    >
                        <img src={quickVariant} alt="photo" className="quickVariant" />
                        <img src={quickVariant} alt="photo" className="quickVariant" />
                        <img src={quickVariant} alt="photo" className="quickVariant" />
                        <img src={quickVariant} alt="photo" className="quickVariant" />
                        
                    </Stack>
                    <button  className="padrobno_btn" > Подробно о товаре</button>
            </Box>
        </Modal>
      </div>
    );
  }
