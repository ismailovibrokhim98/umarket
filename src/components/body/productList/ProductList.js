import React from 'react'
import "./ProductList.css"
// import ProductCard from './productCard/ProductCard';
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import prod1 from "../../../Assets/productList/laptop.png"
import prod2 from "../../../Assets/productList/laptop2.png"
import prod3 from "../../../Assets/productList/laptop3.png"
import prod4 from "../../../Assets/productList/laptop4.png"


function ProductList() {
    return (
        <>
            <div className="main_ProductList">
                <div className="cont_ProductList">
                    <h2 className="productList_title" >Популярные категории</h2>

                    <Box sx={{ flexGrow: 1}}>
                        <Grid container spacing={7}>
                            <Grid item xs={3}>
                                <div className="category_prod">
                                    <div className="category_card">
                                        <img src={prod1} alt="photo" className="category_img" />
                                        <p className="category_title" >Смартфоны</p>
                                    </div>
                                </div>
                            </Grid>

                            <Grid item xs={3}>
                                <div className="category_prod">
                                    <div className="category_card">
                                        <img src={prod2} alt="photo" className="category_img" />
                                        <p className="category_title" >Мониторы</p>
                                    </div>
                                </div>
                            </Grid>

                            <Grid item xs={3}>
                                <div className="category_prod">
                                    <div className="category_card">
                                        <img src={prod3} alt="photo" className="category_img" />
                                        <p className="category_title" >Компьютеры</p>
                                    </div>
                                </div>
                            </Grid>

                            <Grid item xs={3}>
                                <div className="category_prod">
                                    <div className="category_card">
                                        <img src={prod4} alt="photo" className="category_img" />
                                        <p className="category_title" >Аксессуары</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                </div>
            </div>
        </>
    )
}

export default ProductList