import React, { Component } from 'react';
import "./Banner.css"
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import banner1 from "../../../Assets/banner/banner.png"


function Banner() {
    return (
        <>
        <div className="main_banner">
            <div className="cont_banner">
            <Carousel className="banner_carus" autoPlay>
            <div  >
                <img alt="photo" src={banner1} />
                </div>
                <div>
                <img alt="photo" src={banner1} />
                </div>
                <div>
                <img alt="photo" src={banner1} />
                </div>
                <div>
                <img alt="photo" src={banner1} />
                </div>
                <div>
                <img alt="photo" src={banner1} />
                </div>
                <div>
                <img alt="photo" src={banner1} />
                </div>
            </Carousel>
            </div>
        
        </div>
        </>
    )
}

export default Banner
