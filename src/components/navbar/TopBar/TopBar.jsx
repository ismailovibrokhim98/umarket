import React from 'react'
import { Link } from 'react-router-dom'
import { CallIcon, LangIcon, LocIcon } from '../../../Assets/icons/navIcon/NavIcon'
import rusflag from "../../../Assets/icons/navIcon/rusflag.svg"
import "./TopBar.css"


function TopBar() {

    return (
        <>
            <div className="main_topbar">
                <div className="cont_topbar">
                
                    <ul className="topbar_ul " >
                        <Link className="topbar_li" to="/market" >Магазины</Link>
                        <Link className="topbar_li" to="/review" >Оставить отзыв</Link>
                        <Link className="topbar_li" to="/deliver" >Доставка</Link>
                    </ul>
                    
                    <div className="topbar_right">
                        <p className="topbar_call" ><CallIcon/> <span>+998 97 778-17-08</span></p>
                        <p className="topbar_loc" ><LocIcon/> <span>Ташкент</span></p>

                            <button className="lang_btn" >
                                <img src={rusflag} alt="photo" />
                                <span>Рус</span>
                                <LangIcon/>
                            </button>
                         
                    </div>
                </div>
            </div>
        </>
    )
}

export default TopBar