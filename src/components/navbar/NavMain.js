import React from 'react'
import Header from './Header/Header'
import NavBar from './NavBar/NavBar'
import TopBar from './TopBar/TopBar'

function NavMain() {
    return (
        <>
            <TopBar />
            <Header />
            <NavBar />
        </>
    )
}

export default NavMain
