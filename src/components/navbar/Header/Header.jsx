import React from 'react'
import "./Header.css"
import { useSelector, useDispatch } from 'react-redux';
import { toggleLoginModal } from "../../../redux/actions/ModalAction";
import logo from "../../../Assets/icons/navIcon/logo.svg"
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import IconButton from '@mui/material/IconButton';
import { GalleryIcon } from '../../../Assets/icons/icons';

import { ShopIcon } from '../../../Assets/icons/navIcon/NavIcon'
import { HeartIcon } from '../../../Assets/icons/navIcon/NavIcon'
import { DerectionIcon } from '../../../Assets/icons/navIcon/NavIcon'
import { ExitIcon } from '../../../Assets/icons/navIcon/NavIcon'
// import Login from '../registration/Login';
import SignIn from '../registration/SignIn/SignIn';
import { Link } from 'react-router-dom';




function Header() {
const dispatch  =  useDispatch();
const state = useSelector(state => state.loginModal)
    return (
        <>
            <div className="main_header">
                <div className="cont_header">
                <Link to="/"><img src={logo} alt="photo" /></Link>
                <Paper
                component="form"    
                sx={{ p: '2px 14px', display: 'flex', alignItems: 'center', height: "48px", width: "677px", backgroundColor: "#F7F7F7", borderRadius: "4px", boxShadow: "none", ml: "56px", mr: "56px" }}
                >

                    <IconButton type="submit" sx={{ ml: 1, p: '10px' }} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                    
                    <InputBase
                        sx={{ ml: 1, flex: 1 }}
                        placeholder="Поиск по товарам"
                    />
                <GalleryIcon sx={{ cursor: 'click', backgroundColor: "red"}} />
                </Paper>
                
                    <div className="mainNav_btn">
                        <Link to="/korzina"><button className="icn_btns" ><ShopIcon/>Корзина</button></Link>
                        <button className="icn_btns" ><HeartIcon/>Избранные</button>
                       <Link to="/comparison"><button className="icn_btns" ><DerectionIcon/>Сравнение</button></Link> 
                        <button className="icn_btns"
                            onClick={ () => dispatch(toggleLoginModal(!state))}
                        ><ExitIcon/>Войти</button>
                    </div>
                </div>
                    { state ? <SignIn /> : null } 
            </div>
        </>
    )
}

export default Header
