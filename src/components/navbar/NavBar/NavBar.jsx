import React from 'react'
import "./NavBar.css"
import Link from '@mui/material/Link';


function NavBar() {
    return (
        <>
            <div className="main_nav">
                <div className="cont_nav">

                <Link href="#" className="links_NavBar" >Акции и скидки</Link>
                <Link href="#" className="links_NavBar" >Смартфоны и гаджеты</Link>
                <Link href="#" className="links_NavBar" >Телевизоры и аудио</Link>
                <Link href="#" className="links_NavBar" >Техника для кухни</Link>
                <Link href="#" className="links_NavBar" >Красота и здоровье</Link>
                <Link href="#" className="links_NavBar" >Ноутбуки и компьютеры</Link>
                
                </div>
            
            </div>
        </>        
    )
}


export default NavBar
        