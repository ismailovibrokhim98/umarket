import React,{ useState } from 'react'
import "./signIn.css"
import CloseIcon from '@mui/icons-material/Close';
import FormControl, { useFormControl } from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import {GoogleIcon} from '../../../../Assets/icons/icons';
import {ReactComponent as FacebookIcon} from '../../../../Assets/productList/facebook.svg'
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { useSelector, useDispatch } from 'react-redux';
import { toggleLoginModal } from "../../../../redux/actions/ModalAction";
import App from '../../../../App';
import SignUp from '../SignUp/SignUp';

function SignIn({title,type = 'text', ...restProps}) {
    const dispatch  =  useDispatch();
    const state = useSelector(state => state.loginModal)
    const [registr, setRegistr] = useState(false);
    console.log(registr);
    
    
    return (
        <div className="main_register_ovverlay" >
                    <div className="main_register">
                            <CloseIcon onClick={ () => dispatch(toggleLoginModal(!state))} color="disabled" sx={{ position: "absolute", right: "22px"}} />
                            <h2 className="signin_title" >Добро пожаловать</h2>
                            <p className="signin_subtitle"  >Войдите с вашим номером телефона или паролем</p>
                            <FormControl sx={{ width: '423px', mt:"24px" }}>
                                <OutlinedInput type="number" placeholder="+998" sx={{height:"48px", bgcolor:"#F7F7F7"}} />
                                <Button variant="contained" sx={{ height:"48px", background: "#1B5BF7", mt:"12px", fontFamily: "'Poppins',san-serif ", fontSize:"16px" }} >Получить код</Button>
                            </FormControl>
                            <p className="or_enter" >Или войти с</p>
                            <Stack direction="row" spacing={2} sx={{width: '423px', mt:"24px"}} >
                                <Button sx={{width: '200px', height:"48px", borderRadius:"4px", background: "#F7F7F7"}} ><GoogleIcon /></Button>
                                <Button sx={{width: '200px', height:"48px", borderRadius:"4px", background: "#F7F7F7"}} ><FacebookIcon /></Button>
                            </Stack>
                            <p className="or_registr">У вас нет аккаунта?<span><Button onClick={() => setRegistr(true)} >Зарегистрируйтесь</Button></span></p>
                        
                    </div>
                        { registr ? <SignUp /> : null }
                
        </div>
    )
    
}

export default SignIn
