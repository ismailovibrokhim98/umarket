import React from 'react'
import "./SignUp.css"
import CloseIcon from '@mui/icons-material/Close';
import FormControl, { useFormControl } from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import {GoogleIcon} from '../../../../Assets/icons/icons';
import {ReactComponent as FacebookIcon} from '../../../../Assets/productList/facebook.svg'
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Checkbox from '@mui/material/Checkbox';


const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

export default function SignUp({title}) {
  return (
    <div>
        <div className="main_signUp">
                        <CloseIcon color="disabled" sx={{ position: "absolute", right: "22px"}} />
                        <h2 className="signup_title" >Регистрация</h2>
                        <FormControl  sx={{ width: '423px', mt:"24px" }}>
                            <OutlinedInput className="input_forms" type="name" placeholder="Имя" />
                            <OutlinedInput className="input_forms" type="name" placeholder="Фамилия" />
                            <OutlinedInput className="input_forms" type="number" placeholder="Номер телефона"/>
                            <OutlinedInput className="input_forms" type="password" placeholder="Введите пароль" />
                            <OutlinedInput className="input_forms" type="password" placeholder="Подтверждение пароля" />
                            <Button className="btn_regist" variant="contained" >Зарегистрироваться</Button>
                        </FormControl>
                       
                        <Stack direction="row" spacing={2} sx={{width: '423px', mt:"20px"}} >
                             <Checkbox {...label} /> <p className="agreement">Согласен с<span>условиями правил пользования</span> торговой площадкой и <span>правилами возврата</span></p>
                        </Stack>
                        <p className="p_subtit" >Или зарегистрироваться с</p>
                       
                        <Stack direction="row" spacing={2} sx={{width: '423px', mt:"20px"}} >
                            <Button sx={{width: '200px', height:"48px", borderRadius:"4px", background: "#F7F7F7"}} ><GoogleIcon /></Button>
                            <Button sx={{width: '200px', height:"48px", borderRadius:"4px", background: "#F7F7F7"}} ><FacebookIcon /></Button>
                        </Stack>
                        <p className="or_registr">У вас есть аккаунт?<span><Button >Войти</Button></span></p>
        </div>
    </div>
  )
}
