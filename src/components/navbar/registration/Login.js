import React, {useState} from 'react';
import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SignUp';
import { useSelector, useDispatch } from 'react-redux';
import { toggleLoginModal } from "../../../redux/actions/ModalAction";

const Login = () => {
    const [hasAccount, setHasAccount] = useState(false);
    const dispatch  =  useDispatch();
    const modal = useSelector(state => state.loginModal)
    return (
        <div>
            <div
                open = {modal}
                handleClose={ () => dispatch(toggleLoginModal(false))}
                setHasAccount={!hasAccount ? SignIn : SignUp }
                width={550}
            >
                {!hasAccount ?
                 (
                    <SignIn />
                 ) : (  
                    <SignUp />
                 )}

            </div>
        </div>
    );
}

export default Login;
