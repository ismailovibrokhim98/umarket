import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  border: 'none',
  padding: '32px 64px',
  borderRadius: '8px',
};

function BasicModal({ open, setOpen = () => {}, handleClose, title, subTitle, width, children }) {

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box style={{width: `${width}px`}} sx={style}>
            <div className="text-center pb-4 text-2xl font-bold">{title || 'Title'}</div>
            {subTitle && <div className='text-center'>{subTitle}</div>}
            <div className='pt-2'>{children}</div>
        </Box>
      </Modal>
    </div>
  );
}

export default BasicModal;
