import React from "react";
import { FavouriteIcon, VectorTwoIcon } from "../../../Assets/icons/icons";
import { useDispatch } from "react-redux";
import {toggleLoginModal} from '../../../redux/actions/modalAction'
const Product = ({ product }) => {
  const dispatch = useDispatch()
  return (
    <div
      className="product-item flex flex-col items-center p-6 rounded-lg bg-[#fff] shadow-product"
      //   key={product.id}
    >
      <div className="product__image group relative">
        <img className="" src={product.image} alt={product.name} />
        <div className="absolute bottom-1/2 bg-white p-3 rounded-full shadow-productView hidden group-hover:block right-0 left-0 text-center cursor-pointer">
          Быстрый просмотр
        </div>
      </div>
      <div className="product__info mt-8 w-full">
        <h3 className="w-10/12">{product.name}</h3>
        <p className="font-semibold pt-4 pb-2">{product.price}</p>
        <p className="pb-4">{product.loan_price}</p>
      </div>
      <div className="flex justify-between items-center w-full">
        <button className="" onClick={() => dispatch(toggleLoginModal(true))}>Add to cart</button>
        <div className="flex gap-3">
          <span className="cursor-pointer">
            <VectorTwoIcon />
          </span>
          <span className="cursor-pointer">
            <FavouriteIcon />
          </span>
        </div>
      </div>
    </div>
  );
};

export default Product;
