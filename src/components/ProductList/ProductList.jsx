import React from "react";
import Product from "./Product/Product";

const ProductList = ({ title, products }) => {
  if (!products?.length) return null;
  return (
    <div className="product px-5 mt-8 mb-16">
      <div className="container">
        {title ? (
          <h1 className="text-2xl pb-6 font-bold">{title}</h1>
        ) : (
          "This could be title"
        )}
        <div
          className={`grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-8`}
        >
          {products &&
            products.map((product) => (
              <Product key={product.id} product={product} />
            ))}
        </div>
      </div>
    </div>
  );
};

export default ProductList;
