import React from 'react'
import Banner from './body/banner/Banner'
import Brands from './body/brands/Brands'
import HitProductList from './body/hitProduct/HitProductList'
import PlayMarket from './body/playMarket/PlayMarket'
import ProductList from './body/productList/ProductList'
import TechHome from './body/techHome/TechHome'
import FooterNav from './footerNavbar/FooterNav'
import NavMain from './navbar/NavMain'

function Main() {
    return (
        <>

            <NavMain />
            <Banner />
            <ProductList />
            <HitProductList title={'Хиты продаж'} />
            <TechHome />
            <HitProductList title={'Выбор покупателей'} />
            <Brands />
            <PlayMarket />  
            <FooterNav /> 
        </>
    )
}

export default Main
