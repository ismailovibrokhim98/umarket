import React from 'react'
import "./Deliver.css"
import FooterNav from '../../footerNavbar/FooterNav'
import NavMain from '../../navbar/NavMain'

import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';



const deliver_price = [
    {
        name: "Доставка мелкой бытовой техники и электроники",
        price: "30 000 сум",
      },
      
  ];


function Deliver() {
    if (!deliver_price?.length) return null;
    return (
        <>
        <NavMain />
           <div className="main_deliver">
                <div className="cont_deliver">
                    <h1 className="title_deliver" >Доставка</h1>
                    <p className="info_deliver" >Осуществляет  доставку товаров по городу Ташкент. Доставка производится в течение 48 часов с момента подтверждения заказа покупателем.</p>
                    
                        {deliver_price &&
                            deliver_price.map((delivers) => (
                                <div >
                                    
                                    <Box display="flex"
                                        justifyContent="space-between"
                                        alignItems="center"
                                        sx={{mt:"41px"}}
                                        >
                                            <Stack className="deliver_box">
                                                <h4 className="deliver_name">{delivers.name}</h4>
                                                <p className="deliver_price">{delivers.price}</p>
                                            </Stack>
                                            <Stack className="deliver_box">
                                                <h4 className="deliver_name">{delivers.name}</h4>
                                                <p className="deliver_price">{delivers.price}</p>
                                            </Stack>
                                            <Stack className="deliver_box">
                                                <h4 className="deliver_name">{delivers.name}</h4>
                                                <p className="deliver_price">{delivers.price}</p>
                                            </Stack>
                                    </Box>
                                    <Box display="flex"
                                        justifyContent="space-between"
                                        alignItems="center"
                                        sx={{mt:"30px"}}
                                        >
                                            <Stack className="deliver_box">
                                                <h4 className="deliver_name">{delivers.name}</h4>
                                                <p className="deliver_price">{delivers.price}</p>
                                            </Stack>
                                            <Stack className="deliver_box">
                                                <h4 className="deliver_name">{delivers.name}</h4>
                                                <p className="deliver_price">{delivers.price}</p>
                                            </Stack>
                                            <Stack className="deliver_box">
                                                <h4 className="deliver_name">{delivers.name}</h4>
                                                <p className="deliver_price">{delivers.price}</p>
                                            </Stack>
                                    </Box>
                                </div>
                                
                        ))}
                </div>
            </div>
        <FooterNav />
        </>
    )
}

export default Deliver
