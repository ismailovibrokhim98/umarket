import React from 'react'
import "./Review.css"
import FooterNav from '../../footerNavbar/FooterNav'
import NavMain from '../../navbar/NavMain'
import FormControl from '@mui/material/FormControl';   
import InputLabel from '@mui/material/InputLabel'; 
import Input from '@mui/material/Input';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';


function Review() {

    const [market, setMarket] = React.useState('');
    const [tool, setTool] = React.useState('');

    const handleChange = (event) => {
        setMarket(event.target.value);
    };

    
    const onChange = (event) => {
        setTool(event.target.value);
    };


    return (
        <>
            <NavMain />

            <div className="main_review">
                <div className="cont_review">
                    <h1 className="title_review" >Ваш отзыв о Goodzone</h1>
                    <p className="info_review" >Благодарим Вас за то, что Вы посетили наш интернет-магазин. Мы внимательно отслеживаем все этапы работы магазинов нашей торговой сети и будем очень признательны, если вы оставите отзыв о нашей работе.</p>
               
                    <form className='FormGroup' >
                        
                        <InputLabel className='input_label' >  Ф.И.О. </InputLabel>
                        <Input placeholder="Напишите полное имя" className='input_group'  />

                        <InputLabel className='input_label' >Электронный адрес</InputLabel>
                        <Input placeholder="Напишите электронный адрес" className='input_group' type='email'  />

                        <InputLabel className='input_label' >Номер телефона</InputLabel>
                        <Input placeholder="+998" className='input_group' type='number'  />
                       
                        <br/>
                        <InputLabel className='input_label' >Магазин</InputLabel>
                        <FormControl >
                            <Select
                            value={market}
                            onChange={handleChange}
                            displayEmpty
                            className='input_select'
                            >
                                <MenuItem value=""  >
                                    <p className='input_placehol'>Выберите  магазин</p>
                                </MenuItem>
                                <MenuItem value={10}>Goodzone</MenuItem>
                                <MenuItem value={20}>umarket</MenuItem>
                                <MenuItem value={30}>Atlas</MenuItem>
                            </Select>
                        </FormControl>
                       
                       <br/>
                        <InputLabel className='input_label' >Тип отзыва</InputLabel>
                        <FormControl >
                            <Select
                            value={tool}
                            onChange={onChange}
                            displayEmpty
                            className='input_select'
                            >
                                <MenuItem value=""  >
                                    <p className='input_placehol'>Выберите  тип</p>
                                </MenuItem>
                                <MenuItem value={10}>Electronic</MenuItem>
                                <MenuItem value={20}>Laptop</MenuItem>
                                <MenuItem value={30}>Phone</MenuItem>
                            </Select>
                        </FormControl>

                        <InputLabel className='input_label' >Комментарии</InputLabel>
                        <TextField 
                            multiline
                            rows={5}
                            placeholder="Ваш комментарий"
                            className='text_comment'
                        />
                        
                    </form>
                        
                   

                </div>
            </div>

            <FooterNav />
        </>
    )
}

export default Review
