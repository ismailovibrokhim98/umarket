import * as React from 'react';
import NavMain from '../../navbar/NavMain';
import "./market.css"
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import goodzone from "../../../Assets/productList/goodzone.png"
import map from "../../../Assets/productList/map.png"
import { MarketCall_icon, MarketCard_icon, MarketClock_icon, MarketLocation_icon } from '../../../Assets/icons/icons';
import FooterNav from '../../footerNavbar/FooterNav';
const market_info = [
    {
        market: "Goodzone Сергели",
        location1: "г. Ташкент, Сергелийский район, Янги Сергели",
        location2: "Ориентир: Напротив входа в машинный базар",
        workTime: "Часы работы: с 10:00 до 21:00 (без выходных)",
      },
      {
        market: "Goodzone Сергели",
        location1: "г. Ташкент, Сергелийский район, Янги Сергели",
        location2: "Ориентир: Напротив входа в машинный базар",
        workTime: "Часы работы: с 10:00 до 21:00 (без выходных)",
      },
      {
        market: "Goodzone Сергели",
        location1: "г. Ташкент, Сергелийский район, Янги Сергели",
        location2: "Ориентир: Напротив входа в машинный базар",
        workTime: "Часы работы: с 10:00 до 21:00 (без выходных)",
      },
      {
        market: "Goodzone Сергели",
        location1: "г. Ташкент, Сергелийский район, Янги Сергели",
        location2: "Ориентир: Напротив входа в машинный базар",
        workTime: "Часы работы: с 10:00 до 21:00 (без выходных)",
      },
      {
        market: "Goodzone Сергели",
        location1: "г. Ташкент, Сергелийский район, Янги Сергели",
        location2: "Ориентир: Напротив входа в машинный базар",
        workTime: "Часы работы: с 10:00 до 21:00 (без выходных)",
      },
  ];

  

const Market = () => {
    if (!market_info?.length) return null;
    const position = [51.505, -0.09]
    return (
        <>
           <NavMain />
           <div className="main_market">
                <div className="cont_market">
                    <h1 className="title_market" >Магазины</h1>
                    {market_info && 
                        market_info.map((markets) => (
                            <div className="market_box">
                                <h4 className="market_name">{markets.market}</h4>
                                <p className="market_location">{markets.location1}<span>{markets.location2}</span></p>
                                <p className="market_worktime">{markets.workTime}</p>
                            </div>
                            
                    ))}

                    <Box display="flex"
                        justifyContent="space-between"
                        alignItems="flex-start" sx={{ height:"393px", marginTop:"24px"}}
                    >

                        <Box>
                            <img src={goodzone} alt="photo" className="goodzone_img" />
                        
                            <Stack direction="row" spacing={2} sx={{mt:"20px"}} >   
                                <MarketLocation_icon />
                                <p className="location_info" >г. Ташкент, Сергелийский <br/> р-н, Янги Сергели</p>
                            </Stack>

                            <Stack direction="row" spacing={2} sx={{mt:"20px"}} >   
                                <MarketCard_icon />
                                <p className="location_info" >Напротив входа в машинный базар </p>
                            </Stack>

                            <Stack  direction="row" spacing={2} sx={{mt:"20px"}} >   
                                <MarketClock_icon />
                                <p className="location_info" >с 10:00 до 21:00 (без выходных) </p>
                            </Stack>

                            <Stack  direction="row"  spacing={2} sx={{mt:"20px"}} >   
                                <MarketCall_icon />
                                <p className="location_info" >71−207−03−07</p>
                            </Stack>
                        
                        </Box>
                        <Box>
                            <iframe className='map_goodzone' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2998.1261348227276!2d69.34920261566822!3d41.284359210338344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38aef5f9dd57e81d%3A0xa9751988a8858434!2s%22GOODZINE%22%20maishiy%20texnika%20do%E2%80%98koni!5e0!3m2!1sen!2s!4v1651764162332!5m2!1sen!2s"  style={{border:0}} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </Box>
                    
                    </Box>
                        
                        

                </div>
           </div>
           <FooterNav />
        </>
    );
}

export default Market;
