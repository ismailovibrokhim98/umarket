import React from 'react'
import "./oform.css"
import NavMain from '../../navbar/NavMain'
import FormControl from '@mui/material/FormControl';   
import InputLabel from '@mui/material/InputLabel'; 
import Input from '@mui/material/Input';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import payme2 from "../../../Assets/productList/payme2.svg"
import humo2 from "../../../Assets/productList/humo2.svg"
import uzcard2 from "../../../Assets/productList/uzcard2.svg"
import { DeliverHome, DeliverTruck } from '../../../Assets/icons/icons';
import FooterNav from '../../footerNavbar/FooterNav';



const total_order = [
    {
        name: "Samsung Galaxy A41 Red 64 GB",
        price: 320,
    },
    {
        name: "Samsung Galaxy A41 Red 128 GB",
        price: 340,
    },
    {
        name: "Samsung Galaxy A41 Red 256 GB",
        price: 400,
    },
    
  ];


function Oform() {

    const [market, setMarket] = React.useState('');
    const [tool, setTool] = React.useState('');

    const handleChange = (event) => {
        setMarket(event.target.value);
    };

    
    const onChange = (event) => {
        setTool(event.target.value);
    };

    if (!total_order?.length) return null;
    return (
        <>
            <NavMain />
            <div className="main_oform">
                <div className="cont_oform">
                        <h1 className='title_oform_h1' >Оформление заказа</h1>

                    <Box display="flex"
                        alignItems="flex-start"
                        >
                        <Box>
                        <form className='FormGroup' >
                        
                        <InputLabel className='input_label' >  Ф.И.О. </InputLabel>
                        <Input placeholder="Напишите полное имя" className='input_group'  />

                        <InputLabel className='input_label' >Номер телефона</InputLabel>
                        <Input placeholder="+998" className='input_group' type='til'  />
                    
                        
                        <InputLabel className='input_label' >Регион</InputLabel>
                        <FormControl >
                            <Select
                            value={market}
                            onChange={handleChange}
                            displayEmpty
                            className='input_select'
                            >
                                <MenuItem value=""  >
                                    <p className='input_placehol'>Выберите ваш регион</p>
                                </MenuItem>
                                <MenuItem value={10}>Uzbekistan</MenuItem>
                                <MenuItem value={20}>USA</MenuItem>
                                <MenuItem value={30}>Africa</MenuItem>
                            </Select>
                        </FormControl>
                   </form>
                   <p className='mesta_pol' >Выберите своё местоположение на карте</p>

                   <Box sx={{mt:"16px"}}>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2996.36479690085!2d69.18981521566911!3d41.32268020796638!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8dbb8e1f0391%3A0xd04770323fe890a5!2sUdevs!5e0!3m2!1sen!2s!4v1652365893798!5m2!1sen!2s" width="519" height="209" style={{border:0}}
allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                   </Box>

                   <p className='mesta_pol' >Выберите способ оплаты</p>  

                   <Box display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    sx={{width:"517px", height:"104px" ,border:"2px solid #F2F2F2", borderRadius:"4px", mt:"12px" }}>
                        <Stack className='atcksd' >
                            <img src={payme2}  /> 
                        </Stack>
                        <Stack className='border_stack atcksd' >
                             <img src={humo2} /> 
                        </Stack>
                        <Stack className='atcksd' >
                             <img src={uzcard2} /> 
                        </Stack>
                   </Box>

                   <p className='mesta_pol' >Выберите способ доставки</p>  

                   <Box display="flex"
                    alignItems="center"
                    sx={{mt:"12px"}}
                    >
                        <Stack className='dostvakd' >
                            <DeliverTruck /> <span>  Доставка в течении дня</span>
                        </Stack>
                        <Stack className='dostvakd' sx={{ml:"15px"}} >
                             <DeliverHome /> <span>Самовывоз</span>
                        </Stack>
                        
                   </Box>

                <form className='FormGroup' >
                    <InputLabel className='input_label' >Примечания к заказу</InputLabel>
                    <TextField 
                        multiline
                        rows={8}
                        placeholder="Заметки о вашем заказе, например заметки для доставки"
                        className='text_comment text_comments'
                    />
                    
                </form>

            <p className='agree_txt' >Совершая эту покупку, вы соглашаетесь с нашими <span>условиями</span></p>

            <Button sx={{ width:"520px" , height:"56px", mt:"48px", fontSize:"16px", fontWeight:"400px" }} variant='contained' >Оформить заказ</Button>
                        </Box>
                        <Box sx={{ml:"140px"}}>
                            <div className='vashZakaz_box'>
                                        <h3 className='vashZakaz_title' >Ваш заказ</h3>
                                        {total_order && 
                                            total_order.map((orders) => (
                                                <div >
                                                    <div className='orders_list'>
                                                        <p className='order_name'>1x <span>{orders.name}</span></p>
                                                        <p className='order_price'><span>$  </span>{orders.price}</p>
                                                    </div>
                                                    <div className='hr_order'> </div>
                                                </div>
                                                
                                        ))}
                                        <div className='dastavka_sum'> 
                                                <p>Сумма доставки</p>
                                                <p>Бесплатно</p>
                                        </div>
                                        <h3 className='itogo' >Итого <span>$1060</span></h3>
                            </div>
                        </Box>
                    </Box>
                     
                    
                </div>
            </div>

            <FooterNav />
        </>
    )
}

export default Oform
