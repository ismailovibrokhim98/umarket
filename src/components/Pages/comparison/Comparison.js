import * as React from 'react';
import { useState } from 'react';
import "./comparison.css"
import Box from '@mui/material/Box';
import NavMain from '../../navbar/NavMain';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

const total_order = [
    {
        name: "Samsung Galaxy A41 Red 64 GB",
        price: 320,
    },
    {
        name: "Samsung Galaxy A41 Red 128 GB",
        price: 340,
    },
    {
        name: "Samsung Galaxy A41 Red 256 GB",
        price: 400,
    },
    
  ];


function Comparison() {
    const [value, setValue] = React.useState('1');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

  return (
    <div>
        
        <NavMain />
            <div className="main_compare">
                <div className="cont_compare">
                    <div className='compare_title'>Сравнение товаров</div>

                    <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChange} aria-label="lab API tabs example">
                            <Tab label="Кондиционеры" value="1" />
                            <Tab label="Смартфоны" value="2" />
                            <Tab label="Телевизоры" value="3" />
                        </TabList>
                        </Box>
                        <TabPanel value="1">
                            
                                <table className='table_main' style={{width:"1296px"}}>
                                    <tr className='tableMain_row'>
                                        <th > </th>
                                        <th >Цвет</th>
                                        <th >Марка</th>
                                        <th >Площадь (м2)</th>
                                        <th >Управление по Wi-Fi</th>
                                        <th >Вес (кг)</th>
                                        <th >Цена</th>
                                        <th ></th>
                                    </tr>
                                    <tr>
                                        <td>Alfreds Futterkiste</td>
                                        <td>Maria Anders</td>
                                        <td>Germany</td>
                                    </tr>
                                    <tr>
                                        <td>Centro comercial Moctezuma</td>
                                        <td>Francisco Chang</td>
                                        <td>Mexico</td>
                                    </tr>
                                </table>
                            
                        </TabPanel>
                        <TabPanel value="2">Смартфоны</TabPanel>
                        <TabPanel value="3">Телевизоры</TabPanel>
                    </TabContext>
                    </Box>

                </div>
            </div>

    </div>
  )
}

export default Comparison