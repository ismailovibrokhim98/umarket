import React from 'react'
import "./korzina.css"
import FooterNav from '../../footerNavbar/FooterNav'
import NavMain from '../../navbar/NavMain'
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Checkbox from '@mui/material/Checkbox';
import { Button } from '@mui/material';
import { DeleteIcon, RateHeart } from '../../../Assets/icons/icons';
import HitProductList from '../../body/hitProduct/HitProductList';
import { Link } from 'react-router-dom';

export const korzina_items = [
    {
      id: 1,
      name: "Samsung Galaxy A41 Red 64 GB",
      slug: "Потрясающий экран, реальная плавная прокрутка",
      price: "450",
      image: require("../../../Assets/productList/korzinaphone.png"),
    },
    {
        id: 2,
        name: "Samsung Galaxy A41 Red 64 GB",
        slug: "Потрясающий экран, реальная плавная прокрутка",
        price: "450",
        image: require("../../../Assets/productList/korzinaphone.png"),
      },
  ];

function Korzina() {
    
    if (!korzina_items?.length) return null;
    
    return (
        <>
            <NavMain />
            <div className="main_korzina">
                <div className="cont_korzina">
                        <div className='title_korzina'>
                            <h3 className='title_korzina_h3'>Корзина <span>Число товаров: 3</span></h3>
                            <button className='title_korzina_btn' >Удалить выбранные (1)</button>
                        </div>
                   
                        {korzina_items &&
                            korzina_items.map((items) => (
                                <div key={items.id}>
                                            <Box display="flex"
                                                alignItems="flex-start"
                                                sx={{mt:"41px" , width:"846px", height:"192px"  , pt:"31px", pb:"31px" }}
                                                >
                                                
                                                    <Checkbox sx={{mt:"46px"}} />
                                                    <img src={items.image} className="img_korzina" />

                                                    <Stack justifyContent="space-between" sx={{ml:"16px"}}>
                                                        <h4 className='shopmap_h4' >{items.name}</h4>
                                                        <p className='shopmap_p' >{items.slug}</p>

                                                        <Stack 
                                                            direction= "row"
                                                            sx={{mt:"48px"}}
                                                        >
                                                            <button className='btn_map_karzinka' > <RateHeart /> <span>В избранные</span></button>
                                                            <button className='btn_map_karzinka' > <DeleteIcon /> <span>Удалить</span></button>
                                                        </Stack>
                                                    </Stack>
                                                    

                                                    <Stack direction= "Column" alignItems="flex-end" sx={{ml:"180px"}} >
                                                        <Stack
                                                            direction= "row"
                                                            display="flex"
                                                            alignItems="center"
                                                        >
                                                            <button className='increas_btn'>+</button>
                                                            <span className='increas_span' >1</span>
                                                            <button className='increas_btn'>-</button>
                                                        </Stack>

                                                        <Stack
                                                            sx={{mt:"32px"}}
                                                        >
                                                            <span>$ {items.price}</span>
                                                        </Stack>
                                                    </Stack>
                                            </Box>
                                      
                                </div>
                                
                        ))}

                        <div className='amount_price' >
                            <Stack>
                                <h5 className='amount_h5'>Итого <span>$567</span></h5>
                                <p className='amount_p' >Товары: <span>3 шт</span></p>
                                <p className='amount_p' >Скидка:  <span>-$10</span></p>
                                <p className='amount_p' >Доставка: <span className='amount_span'>Выбрать адрес доставки</span></p>
                                <button className='amount_btn'><Link className='LinkM' to="/oform" >Перейти к оформлению</Link></button>
                                <p className='title_sapn' >Заказывая соглашаетесь с условиями пользовании торговой площадкой</p>
                            </Stack>
                        </div>
                </div>  
            </div>

            <HitProductList title="Ещё вам может понравится" />
            <FooterNav />
        </>
    )
}

export default Korzina
