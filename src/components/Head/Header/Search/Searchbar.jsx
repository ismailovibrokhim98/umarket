import React from "react";
import { GalleryIcon, SearchIcon } from "../../../../Assets/icons/icons";

const Searchbar = () => {
  return (
    <div className="search h-12 flex items-center gap-3 grow bg-[#F6F6F6] px-4 rounded-sm">
      <div className="cursor-pointer">
        <SearchIcon />
      </div>
      <input
        className="flex-1 border-none py-1 bg-[#F6F6F6]"
        type="text"
        placeholder="Поиск по товарам"
      />
      <div className="cursor-pointer">
        <GalleryIcon />
      </div>
    </div>
  );
};

export default Searchbar;
