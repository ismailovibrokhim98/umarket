import React from "react";
import { useDispatch } from "react-redux";
import { toggleLoginModal } from "../../../../redux/actions/modalAction";
import {
  EnterIcon,
  FavouriteIcon,
  ShoppingCartIcon,
  VectorTwoIcon,
} from "../../../../Assets/icons/icons";

const Actions = () => {
  const dispatch = useDispatch();
  return (
    <div className="actions flex items-center gap-6">
      <div className="flex flex-col items-center cursor-pointer">
        <ShoppingCartIcon />
        <div className="action__content">Корзина</div>
      </div>
      <div className="flex flex-col items-center cursor-pointer">
        <FavouriteIcon />
        <div className="action__content">Избранные</div>
      </div>
      <div className="flex flex-col items-center cursor-pointer">
        <VectorTwoIcon />
        <div className="action__content">Сравнение</div>
      </div>
      <div
        className="flex flex-col items-center cursor-pointer"
        onClick={() => dispatch(toggleLoginModal(true))}
      >
        <EnterIcon />
        <div className="action__content">Войти</div>
      </div>
    </div>
  );
};

export default Actions;
