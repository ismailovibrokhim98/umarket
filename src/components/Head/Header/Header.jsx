import React from "react";
import "./header.css";
import { ReactComponent as LogoIcon } from "../../../Assets/icons/Logo.svg";
import Searchbar from "./Search/Searchbar";
import Actions from "./Actions/Actions";
import Login from "../../Login/Login";

const Header = () => {
  return (
    <div className="header px-5">
      <div className="container flex justify-between items-center h-full gap-14">
        <div className="cursor-pointer">
          <LogoIcon />
        </div>
        <Searchbar />
        <Actions />
        <Login/>
      </div>
    </div>
  );
};

export default Header;
