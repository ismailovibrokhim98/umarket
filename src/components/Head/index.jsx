import React from 'react'
import Topbar from './Topbar/Topbar'
import Header from './Header/Header';
import Navbar from './Navbar/Navbar'

const Head = () => {
    return (
        <>
        <Topbar />
        <Header />
        <Navbar/>
        </>
    );
}
 
export default Head;