import React from "react";
import { Phone } from "../../../Assets/icons/icons";
// import Language from "./Language/Language";
// import Location from "./Location/Location";
// import "./topbar.css";

const Topbar = () => {
  const lists = [
    {
      id: 1,
      name: "Акции и скидки",
      link: "#",
    },
    {
      id: 2,
      name: "Смартфоны и гаджеты",
      link: "#",
    },
    {
      id: 3,
      name: "Телевизоры и аудио",
      link: "#",
    },
    {
      id: 4,
      name: "Техника для кухни",
      link: "#",
    },
    {
      id: 5,
      name: "Красота и здоровье",
      link: "#",
    },
    {
      id: 6,
      name: "Ноутбуки и компьютеры",
      link: "#",
    },
  ];
  return (
    <div className="h-10 border-b px-5 bg-[#f5f5f5]">
      <div className="container flex justify-between items-center h-full">
        <div className="flex items-center h-full">
          {lists.map((list) => (
            <li key={list?.id} className="pr-8 last:pr-0">
              <a href={list?.link} className="text-sm">
                {list?.name}
              </a>
            </li>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Topbar;
