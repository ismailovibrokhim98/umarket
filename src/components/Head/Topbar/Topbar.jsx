import React from "react";
import { Phone } from "../../../Assets/icons/icons";
import Language from "./Language/Language";
import Location from "./Location/Location";
import "./topbar.css";

const Topbar = () => {
  const lists = [
    {
      id: 1,
      name: "Магазины",
      link: "/shops",
    },
    {
      id: 2,
      name: "Оставить отзыв",
      link: "/reviews",
    },
    {
      id: 3,
      name: "Доставка",
      link: "/delivery",
    },
  ];
  return (
    <div className="h-10 border-b px-5">
      <div className="container flex justify-between items-center h-full">
        <div className="flex items-center h-full">
          {lists.map((list) => (
            <li key={list?.id} className="pr-8 last:pr-0">
              <a href={list?.link} className="text-sm">
                {list?.name}
              </a>
            </li>
          ))}
        </div>
        <div className="">
            <div className="flex items-center h-full">
                <li className="topbar__phone flex items-center pr-8 last:pr-0">
                    <Phone/>
                    <a href="tel:+998 97 778-17-17" className="text-sm">+998 97 778-17-17</a>
                </li>
                <Location />
                <Language/>
            </div>
        </div>
      </div>
    </div>
  );
};

export default Topbar;
