import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Mark } from "../../../../Assets/icons/icons";
import { changeLocation } from "../../../../redux/actions/settingAction";

const Location = () => {
  const location = useSelector((state) => state.setting.location);
  const lang = useSelector((state) => state.setting.language);
  const dispatch = useDispatch();
  console.log("Location:", location);

  const getOptionByLang = (language) => {
    switch (language) {
      case "ru":
        return [
          {
            value: "tashkent",
            label: "Ташкент",
          },
          {
            value: "samarkand",
            label: "Самарканд",
          },
        ];
      case "uz":
        return [
          {
            value: "tashkent",
            label: "Tashkent",
          },
          {
            value: "samarkand",
            label: "Samarkand",
          },
        ];
      default:
        return [
          {
            value: "tashkent",
            label: "Ташкент",
          },
          {
            value: "samarkand",
            label: "Самарканд",
          },
        ];
    }
  };
  return (
    <div className="flex pr-8">
      <Mark />
      <select
        className="ml-2 cursor-pointer"
        defaultValue={location}
        onChange={(event) => dispatch(changeLocation(event.target.value))}
      >
        {getOptionByLang(lang).map((item, index) => (
          <option key={index} value={item.value}>
            {item.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Location;
