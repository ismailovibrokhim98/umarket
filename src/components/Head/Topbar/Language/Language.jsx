import React from "react";
import { Lang } from "../../../../Assets/icons/icons";
import { useSelector, useDispatch } from "react-redux";
import { changeLanguage } from "../../../../redux/actions/settingAction";

const Language = () => {
  const lang = useSelector((state) => state.setting.language);
  const dispatch = useDispatch();
  console.log('Lang:', lang);
  return (
    <div className="flex">
      <Lang />
      <select
        className="ml-2 cursor-pointer"
        defaultValue={lang}
        onChange={(event) => dispatch(changeLanguage(event.target.value))}
      >
        <option value="ru">Рус</option>
        <option value="uz">Uzb</option>
      </select>
    </div>
  );
};

export default Language;
