import React from "react";

const FInput = ({type = 'text', ...restProps}) => {
  return (
    <div className="p-4 my-3 rounded bg-[#F7F7F7]">
      <input className="bg-[#F7F7F7] w-full outline-none" type={type} {...restProps}/>
    </div>
  );
};

export default FInput;
