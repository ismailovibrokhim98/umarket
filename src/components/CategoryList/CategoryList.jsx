import React from "react";

const CategoryList = ({ title, products, width = 300, column = 4 }) => {
  if (!products?.length) return null;
  return (
    <div className="product px-5 mt-8 mb-16">
      <div className="container">
        {title ? (
          <h1 className="text-2xl pb-6 font-bold">{title}</h1>
        ) : (
          "This could be title"
        )}
        <div
          // className={`product__holder grid lg:grid-cols-${column ? column : 4} md:grid-cols-3 sm:grid-cols-2 gap-8`}
          className={`product__holder grid gap-7 justify-center`}
          style={{ gridTemplateColumns: `repeat(auto-fit, ${width}px)` }}
        >
          {products &&
            products.map((product) => (
              <div
                className="product-item flex flex-col items-center p-4 rounded-lg bg-[#f5f5f5]"
                key={product.id}
              >
                <div className="product__image">
                  <img src={product.image} alt={product.name} />
                </div>
                <div className="product__info font-bold mt-8">
                  <h3 className=" text-lg">{product.name}</h3>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default CategoryList;
