import React from 'react'
import "./FooterNav.css"
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Link } from '@mui/material';
import Stack from '@mui/material/Stack';
import payment from "../../Assets/productList/payment.png"
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';

function FooterNav() {
    return (
        <>
            <div className="mainFooterNav">
                <div className="contFooterNav">
                   
                    <div className="footerTopNav">
                        <Box  sx={{ flexGrow: 1}}>
                            <Grid sx={{ display: "flex", justifyContent: "space-between"}} spacing={7}>
                                <Grid item xs={3}>
                                    <h2 className='link_title' >Компания</h2>
                                    <Stack direction="column" spacing={1}>
                                        <Link className="link_footerNav" >О компании</Link>
                                        <Link className="link_footerNav" >Адреса магазинов</Link>
                                    </Stack>
                                    
                                </Grid>
                                <Grid item xs={3}>
                                     <h2 className='link_title'>Информация</h2>
                                    <Stack direction="column" spacing={1}>
                                        <Link className="link_footerNav" >Рассрочка</Link>
                                        <Link className="link_footerNav" >Доставка </Link>
                                        <Link className="link_footerNav" >Бонусы</Link>
                                    </Stack>
                                </Grid>
                                <Grid item xs={3}>
                                    <h2 className='link_title'>Помощь покупателю</h2>
                                    <Stack direction="column" spacing={1}>
                                        <Link className="link_footerNav" >Вопросы и ответы</Link>
                                        <Link className="link_footerNav" >Как сделать заказ на сайте </Link>
                                        <Link className="link_footerNav" >Обмен и возврат</Link>
                                    </Stack>
                                </Grid>
                                <Grid item xs={3}>
                                     <h2 className='link_title'>Способ  оплаты</h2>
                                    <Stack className="saf" direction="row" spacing={1}>
                                        <img src={payment} alt="photo" />
                                    </Stack>
                                </Grid>
                                <Grid item xs={3}>
                                     <h2 className='link_title'>Мы в социальных сетях</h2>
                                    <Stack className="saf" direction="row" spacing={1}>
                                        <LinkedInIcon className="linkedn" />
                                        <InstagramIcon className="instagram" />
                                        <TwitterIcon className="twiter" />
                                        <FacebookIcon className="facebook" />
                                    </Stack>
                                </Grid>
                            </Grid>
                        </Box>
                    </div>
                   
                    <div className="footerBottomNav">
                        <div className="footerinf1">
                            <h3>Единый кол центр</h3>
                            <p>+9980 71-54-60-60</p>
                        </div>
                        <div className="footerinf2">
                            <h3>Почта для пожеланий и предложений</h3>
                            <p>info@udevsmarket.com</p>
                        </div>

                    </div>
                        <p className="bottom_secr" ><span>UdevsMarket.uz</span>  Все права защищены</p>
                </div>
            </div>
        </>
    )
}

export default FooterNav
