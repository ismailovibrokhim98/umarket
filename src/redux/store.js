import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers/rootReducer";
import ModalReducer from "./reducers/ModalReducer";

export const store = createStore(ModalReducer, composeWithDevTools());


