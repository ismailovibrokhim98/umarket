const initialState = {
    loginModal:false,
};

const ModalReducer = ( state = initialState,action ) => {
    switch (action.type) {
        case "TOGGLE_LOGIN_MODAL":
           return{
               ...state,
               loginModal:action.payload,
           };
        default:
           return state;
    }
}

export default ModalReducer;