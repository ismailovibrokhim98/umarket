import { combineReducers } from "redux";
import ModalReducer from "./ModalReducer.js"

const rootReducer = combineReducers({
    ModalReducer: ModalReducer
});

export default rootReducer;