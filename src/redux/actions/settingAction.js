export const changeLanguage = (language) => {
    return {
        type: 'SET_LANGUAGE',
        payload: language
    }
}
export const changeLocation = (location) => {
    return {
        type: 'SET_LOCATION',
        payload: location
    }
}