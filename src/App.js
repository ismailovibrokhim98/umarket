import React, { useEffect } from 'react';
import './App.css'
// ! import request instead of axios
import { request } from './utils/request';
import NavMain from './components/navbar/NavMain';
import Banner from './components/body/banner/Banner';
import ProductList from './components/body/productList/ProductList';
import HitProductList from './components/body/hitProduct/HitProductList';
import TechHome from './components/body/techHome/TechHome';
import Brands from './components/body/brands/Brands';
import PlayMarket from './components/body/playMarket/PlayMarket';
import FooterNav from './components/footerNavbar/FooterNav';
import SignIn from './components/navbar/registration/SignIn/SignIn';
import SignUp from './components/navbar/registration/SignUp/SignUp';
import Header from './components/navbar/Header/Header';
import Login from './components/navbar/registration/Login';
import QuickLook from './components/body/productList/quickLook/QuickLook';
import Market from './components/Pages/market/Market';
import Deliver from './components/Pages/deliver/Deliver';
import Review from './components/Pages/review/Review';
import Main from './components/Main';
import NavRouts from './routs/NavRouts';


const App = () => {
  const getUsers = () => {
    request.get('/users')
      .then(res => {
        // console.log('res', res);
      })
      .catch(err => {
        // console.log('err', err);
      });
  };

  useEffect(() => {
    getUsers();
  }, []);
  
  return (
    <div>
      <div className='container_all' >
          <NavRouts />
          
      </div> 
     
    </div>
  );
}
 
export default App;