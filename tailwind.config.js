module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      boxShadow: {
        product: "0px 0px 75px rgba(0, 0, 0, 0.08);",
        productView: "0px 0px 43px rgba(0, 0, 0, 0.15);" 
      }
    },
  },
  plugins: [],
}
